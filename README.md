# Testing task for QA Automation


## Automated test project for the tester.so utility

### To run:

pip install -r requirements.txt 

pytest --alluredir=allure-results tests/

allure serve allure-results/



###
some tests are skipped because the application crashes after them
