import pytest_asyncio
import websockets


host = 'ws://127.0.0.1:4000'


@pytest_asyncio.fixture
async def websocket_connection():
    async with websockets.connect(host) as websocket:
        yield websocket
