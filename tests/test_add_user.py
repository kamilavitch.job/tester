import json

import allure
import pytest
from factory.utils import get_id, generate_phone, generate_age, get_name


@allure.epic('Check add user')
class TestAddUser:

    @allure.description('Check adding new user')
    @pytest.mark.asyncio
    async def test_add_new_user_positive(self, websocket_connection):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'success'

    @allure.description('Check adding user twice times')
    @pytest.mark.asyncio
    async def test_add_user_exist(self, websocket_connection):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('status') == 'success'
        with allure.step('Send request add user again'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer add user again'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check adding user with not unique phone')
    @pytest.mark.asyncio
    async def test_add_user_with_same_phone(self, websocket_connection):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send first request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('status') == 'success'
        with allure.step('Send request add user with not unique id'):
            request_data_new = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': request_data['phone'],
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(request_data_new))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data_new.get('id')
            assert response_json.get('method') == request_data_new.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check adding user with incorrect phone')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('phone', [None, '', 7906749, -0, ' ', 'Qwerty$325', '*$'],
                             ids=['None', 'empty str', 'int', 'negative', 'space', 'not_digits', 'regex'])
    async def test_add_user_with_incorrect_phone(self, websocket_connection, phone):
        id = get_id()
        with allure.step('Delete phone from db'):
            request_data = {
                'id': id,
                'method': 'delete',
                'phone': phone
            }
            await websocket_connection.send(json.dumps(request_data))
            await websocket_connection.recv()
        with allure.step('Prepare data'):
            request_data_add = {
                'id': id,
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': phone,
                'age': generate_age()
            }
        with allure.step('Send first request'):
            await websocket_connection.send(json.dumps(request_data_add))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data_add.get('id')
            assert response_json.get('method') == request_data_add.get('method')
            assert response_json.get('status') == 'failure', 'phone value should be validated'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check adding new user with incorrect id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', [None, 7906749, -1],
                             ids=['None', 'int', 'negative'])
    async def test_add_user_incorrect_id(self, websocket_connection, id):
        with allure.step('Prepare data'):
            request_data = {
                'id': id,
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id'), 'message should contain id'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check adding new user with empty string id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', ['', ' '],
                             ids=['empty_str', 'space'])
    async def test_add_user_invalid_id(self, websocket_connection, id):
        with allure.step('Prepare data'):
            request_data = {
                'id': id,
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check adding new user with incorrect age')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('age', [None, '',  -1, ' ', '&8()'],
                             ids=['None', 'empty str', 'negative', 'space', 'symbols'])
    async def test_add_user_incorrect_age(self, websocket_connection, age):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': age
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method', None) == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check adding new user with incorrect method')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('method', [None, '',  ' ', '&8()', 'get'],
                             ids=['None', 'empty str', 'space', 'symbols', 'non_exist_method'])
    async def test_add_user_incorrect_method(self, websocket_connection, method):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': method,
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method') == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'

    @allure.description('Check adding new user with incorrect name')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('name', [None, '', 123455],
                             ids=['None', 'empty str', 'int'])
    async def test_add_user_incorrect_name(self, websocket_connection, name):
        with allure.step('Prepare data'):
            request_data = {
                'id': get_id(),
                'method': 'add',
                'name': name,
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
        with allure.step('Send request'):
            await websocket_connection.send(json.dumps(request_data))
        with allure.step('Get response'):
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method') == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'
