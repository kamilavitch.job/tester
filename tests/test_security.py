import json

import allure
import pytest
from factory.utils import get_id, generate_phone, generate_age, get_name


@allure.epic('Security tests')
class TestSecurity:

    @allure.description('Check sql injection')
    @pytest.mark.asyncio
    async def test_sql_injection(self, websocket_connection):
        with allure.step('Create new user with sql injection'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': f'{get_name().split()[0]}; DROP TABLE users; --',
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'failure', 'sql injections should be validated'

    @allure.description('Check XSS vulnerability')
    @pytest.mark.asyncio
    async def test_xss_vulnerability(self, websocket_connection):
        with allure.step('Create new user with xss vulnerability'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': '<script>alert(123)</script>',
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'failure', 'xss vulnerability should be validated'
