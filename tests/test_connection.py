import asyncio
import json

import allure
import pytest
import websockets
from factory.utils import get_id, generate_phone

host = 'ws://127.0.0.1:4000'


@allure.epic('Connection tests')
class TestConnection:

    @allure.description('Check connection is opened')
    @pytest.mark.asyncio
    async def test_connection_open(self, websocket_connection):
        with allure.step('Check connection'):
            assert websocket_connection.open
            assert 'State.OPEN' in str(websocket_connection.state)
        with allure.step('Select phone from db'):
            request_data = {
                'id': get_id(),
                'method': 'select',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check connection is closed')
    @pytest.mark.asyncio
    async def test_connection_close(self, websocket_connection):
        with allure.step('Close connection'):
            await websocket_connection.close()
        with allure.step('Check connection'):
            assert websocket_connection.open is False
            assert 'State.CLOSED' in str(websocket_connection.state)

    @allure.description('Check not informed message')
    @pytest.mark.asyncio
    async def test_check_not_informed_message(self, websocket_connection):
        with allure.step('Send symple message'):
            await websocket_connection.send('hello')
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @pytest.mark.skip(reason='failed to read from socket, error: asio.misc:2')
    @allure.description('Check several connections')
    @pytest.mark.asyncio
    async def test_websocket_multiple_connections(self):
        num_clients = 10
        with allure.step(f'Open {num_clients} connections'):
            clients = [websockets.connect(host) for _ in range(num_clients)]
            responses = await asyncio.gather(*clients)
        with allure.step('Check answer'):
            for response in responses:
                assert response.open, 'Websocket connection failed.'

    @pytest.mark.skip(reason='failed to read from socket, error: asio.misc:2')
    @allure.description('Check large number of websocket connections')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('num_clients', [100, 500],
                             ids=['one hundred clients', 'five hundred clients'])
    async def test_large_number_of_websocket_connections(self, num_clients):
        with allure.step(f'Open {num_clients} connections'):
            clients = [websockets.connect(host) for i in range(num_clients)]
            responses = await asyncio.gather(*clients)
        with allure.step('Check answer'):
            for response in responses:
                assert response.status == 101, f'Failed to connect {response.reason}'
                assert response.open, 'Websocket connection failed.'
