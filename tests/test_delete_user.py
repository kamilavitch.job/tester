import json

import allure
import pytest
from factory.utils import get_id, generate_phone, generate_age, get_name


@allure.epic('Check delete user')
class TestDeleteUser:

    @allure.description('Check deleting new user')
    @pytest.mark.asyncio
    async def test_delete_user_positive(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Delete phone from db'):
            request_data = {
                'id': data['id'],
                'method': 'delete',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check deleting not exist user')
    @pytest.mark.asyncio
    async def test_delete_user_not_exist(self, websocket_connection):
        with allure.step('Delete phone from db'):
            request_data = {
                'id': get_id(),
                'method': 'delete',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

        with allure.step('Check user from db'):
            request_data = {
                'id': request_data['id'],
                'method': 'select',
                'phone': request_data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check deleting with incorrect id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', [None, 7906749, -1],
                             ids=['None', 'int', 'negative'])
    async def test_delete_user_incorrect_id(self, websocket_connection, id):
        with allure.step('Delete phone from db'):
            request_data = {
                'id': id,
                'method': 'delete',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id'), 'message should contain id'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check deleting with empty string id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', ['', ' '],
                             ids=['empty_str', 'space'])
    async def test_delete_user_empty_id(self, websocket_connection, id):
        with allure.step('Delete phone from db'):
            request_data = {
                'id': id,
                'method': 'delete',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check deleting with incorrect method')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('method', [None, '',  ' ', '&8()', 'get'],
                             ids=['None', 'empty str', 'space', 'symbols', 'non_exist_method'])
    async def test_delete_user_incorrect_method(self, websocket_connection, method):
        with allure.step('Delete phone from db'):
            request_data = {
                'id': get_id(),
                'method': method,
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method') == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'

    @allure.description('Check deleting with incorrect phone')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('phone', [None, '', 7906749, -0, ' ', 'Qwerty$325', '*$'],
                             ids=['None', 'empty str', 'int', 'negative', 'space', 'not_digits', 'regex'])
    async def test_delete_user_incorrect_phone(self, websocket_connection, phone):
        with allure.step('Delete phone from db'):
            request_data = {
                'id': get_id(),
                'method': 'delete',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id'), 'message should contain id'
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check deleting user correct phone new id')
    @pytest.mark.asyncio
    async def test_delete_user_correct_phone_new_id(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Delete phone from db'):
            request_data = {
                'id': get_id(),
                'method': 'delete',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

        with allure.step('Check phone from db'):
            select_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == select_data.get('id')
            assert response_json.get('method') == select_data.get('method')
            assert response_json.get('status') == 'success'

    @allure.description('Check deleting user correct phone incorrect id')
    @pytest.mark.asyncio
    async def test_delete_user_correct_id_new_phone(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            resp = await websocket_connection.recv()
            resp_json = json.loads(resp)
            assert resp_json.get('status') == 'success'

        with allure.step('Delete phone from db'):
            request_data = {
                'id': data['id'],
                'method': 'delete',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

        with allure.step('Check user from db'):
            select_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(select_data))
            response_select = await websocket_connection.recv()
            response_json_select = json.loads(response_select)
        with allure.step('Check answer'):
            assert response_json_select.get('id') == select_data.get('id')
            assert response_json_select.get('method') == select_data.get('method')
            assert response_json_select.get('status') == 'success'
