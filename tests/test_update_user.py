import json

import allure
import pytest
from factory.utils import get_id, generate_phone, generate_age, get_name, generate_string


@allure.epic('Check update user')
class TestUpdateUser:

    @allure.description('Check updating age and name new user')
    @pytest.mark.asyncio
    async def test_update_new_user_name_age(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update new user'):
            request_data = {
                'id': data['id'],
                'method': 'update',
                'name': generate_string(),
                'surname': data['surname'],
                'phone': data['phone'],
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check update answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'success'
        with allure.step('Get updated user from db'):
            select_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(select_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check that user updated'):
            assert response_json.get('users')[0].get('name') == request_data.get('name')
            assert response_json.get('users')[0].get('age') == request_data.get('age'), 'age value do not updated'

    @allure.description('Check updating name and surname new user')
    @pytest.mark.asyncio
    async def test_update_new_user_name_surname(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update new user name and surname'):
            request_data = {
                'id': get_id(),
                'method': 'update',
                'name': generate_string(),
                'surname': generate_string(),
                'phone': data['phone'],
                'age': data['age']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check update answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'success'
        with allure.step('Get updated user from db'):
            select_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(select_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check that user updated'):
            assert response_json.get('users')[0].get('name') == request_data.get('name')
            assert response_json.get('users')[0].get('surname') == request_data.get('surname')

    @allure.description('Check update not exist user')
    @pytest.mark.asyncio
    async def test_update_not_exist_user(self, websocket_connection):
        with allure.step('Update not exist user'):
            request_data = {
                'id': get_id(),
                'method': 'update',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check update answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check updating phone exist user')
    @pytest.mark.asyncio
    async def test_update_user_phone(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update user phone'):
            request_data = {
                'id': data['id'],
                'method': 'update',
                'name': data['name'],
                'surname': data['surname'],
                'phone': generate_phone(),
                'age': data['age']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check update answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check updating user without required field')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('param', ['name', 'surname'],
                             ids=['by_name', 'by_surname'])
    async def test_update_new_user_no_required_field(self, websocket_connection, param):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update new user'):
            request_data = {
                'id': data['id'],
                'method': 'update',
                'phone': data['phone'],
                param: generate_string()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check update answer'):
            assert response_json.get('id') == request_data.get('id'), 'message should contain id'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check select with incorrect id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', [None, 7906749, -1],
                             ids=['None', 'int', 'negative'])
    async def test_select_user_incorrect_id(self, websocket_connection, id):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update new user'):
            request_data = {
                'id': id,
                'method': 'update',
                'name': generate_string(),
                'surname': data['surname'],
                'phone': data['phone'],
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id'), 'message should contain id'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check update with incorrect method')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('method', [None, '',  ' ', '&8()', 'get'],
                             ids=['None', 'empty str', 'space', 'symbols', 'non_exist_method'])
    async def test_update_user_incorrect_method(self, websocket_connection, method):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Update new user'):
            request_data = {
                'id': data['id'],
                'method': method,
                'name': generate_string(),
                'surname': data['surname'],
                'phone': data['phone'],
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method') == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'
