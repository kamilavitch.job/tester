import json

import allure
import pytest
from factory.utils import get_id, generate_phone, generate_age, get_name, generate_string


@allure.epic('Check select user')
class TestSelectUser:

    @allure.description('Check select new user by phone')
    @pytest.mark.asyncio
    async def test_select_user_by_phone(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'phone': data['phone']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert len(response_json.get('users')) == 1
            assert response_json.get('users')[0].get('name') == data.get('name')
            assert response_json.get('users')[0].get('phone') == data.get('phone')
            assert response_json.get('users')[0].get('surname') == data.get('surname')
            assert response_json.get('users')[0].get('age') == data.get('age'), 'incorrect age'
            assert response_json.get('status') == 'success', 'incorrect status'

    @allure.description('Check select new user by surname')
    @pytest.mark.asyncio
    async def test_select_one_user_by_surname(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': generate_string(),
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'surname': data['surname']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert len(response_json.get('users')) == 1
            assert response_json.get('users')[0].get('name') == data.get('name')
            assert response_json.get('users')[0].get('phone') == data.get('phone')
            assert response_json.get('users')[0].get('surname') == data.get('surname')
            assert response_json.get('users')[0].get('age') == data.get('age'), 'incorrect age'
            assert response_json.get('status') == 'success', 'incorrect status'

    @allure.description('Check select new user by name')
    @pytest.mark.asyncio
    async def test_select_one_user_by_name(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': generate_string(),
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'name': data['name']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert len(response_json.get('users')) == 1
            assert response_json.get('users')[0].get('name') == data.get('name')
            assert response_json.get('users')[0].get('phone') == data.get('phone')
            assert response_json.get('users')[0].get('surname') == data.get('surname')
            assert response_json.get('users')[0].get('age') == data.get('age'), 'incorrect age'
            assert response_json.get('status') == 'success', 'incorrect status'

    @allure.description('Check select several users by surname')
    @pytest.mark.asyncio
    async def test_select_users_by_surname(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Create second user'):
            data_2 = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': data['surname'],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data_2))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'surname': data['surname']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('status') == 'success', 'incorrect status'
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert len(response_json.get('users')) >= 2, 'incorrect count of users'
            for i in range(len(response_json.get('users'))):
                if response_json.get('users')[i].get('phone') == data.get('phone'):
                    assert response_json.get('users')[i].get('name') == data.get('name')
                    assert response_json.get('users')[i].get('surname') == data.get('surname')
                    assert response_json.get('users')[i].get('phone') == data.get('phone')
                    assert response_json.get('users')[i].get('age') == data.get('age'), 'incorrect age'
                elif response_json.get('users')[i].get('phone') == data_2.get('phone'):
                    assert response_json.get('users')[i].get('name') == data_2.get('name')
                    assert response_json.get('users')[i].get('surname') == data_2.get('surname')
                    assert response_json.get('users')[i].get('phone') == data_2.get('phone')
                    assert response_json.get('users')[i].get('age') == data_2.get('age'), 'incorrect age'

    @allure.description('Check select several users by name')
    @pytest.mark.asyncio
    async def test_select_users_by_name(self, websocket_connection):
        with allure.step('Create new user'):
            data = {
                'id': get_id(),
                'method': 'add',
                'name': get_name().split()[0],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Create second user'):
            data_2 = {
                'id': get_id(),
                'method': 'add',
                'name': data['name'],
                'surname': get_name().split()[1],
                'phone': generate_phone(),
                'age': generate_age()
            }
            await websocket_connection.send(json.dumps(data_2))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
            assert response_json.get('status') == 'success'

        with allure.step('Check user from db'):
            request_data = {
                'id': data['id'],
                'method': 'select',
                'name': data['name']
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('status') == 'success', 'incorrect status'
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert len(response_json.get('users')) >= 2, 'incorrect count of users'
            for i in range(len(response_json.get('users'))):
                if response_json.get('users')[i].get('phone') == data.get('phone'):
                    assert response_json.get('users')[i].get('name') == data.get('name')
                    assert response_json.get('users')[i].get('surname') == data.get('surname')
                    assert response_json.get('users')[i].get('phone') == data.get('phone')
                    assert response_json.get('users')[i].get('age') == data.get('age'), 'incorrect age'
                elif response_json.get('users')[i].get('phone') == data_2.get('phone'):
                    assert response_json.get('users')[i].get('name') == data_2.get('name')
                    assert response_json.get('users')[i].get('surname') == data_2.get('surname')
                    assert response_json.get('users')[i].get('phone') == data_2.get('phone')
                    assert response_json.get('users')[i].get('age') == data_2.get('age'), 'incorrect age'

    @allure.description('Check select not exist user')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('param', ['phone', 'name', 'surname'],
                             ids=['by_phone', 'by_name', 'by_surname'])
    async def test_select_user_not_exist(self, websocket_connection, param):
        with allure.step('Check user from db'):
            request_data = {
                'id': get_id(),
                'method': 'select',
                param: generate_string()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure', 'incorrect status'

    @allure.description('Check select with incorrect id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', [None, 7906749, -1],
                             ids=['None', 'int', 'negative'])
    async def test_select_user_incorrect_id(self, websocket_connection, id):
        with allure.step('Select phone from db'):
            request_data = {
                'id': id,
                'method': 'select',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id'), 'message should contain id'
            assert response_json.get('status') == 'failure'
            assert response_json.get('reason', '') != '', 'in case of failure needs reason'

    @allure.description('Check select with empty string id')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('id', ['', ' '],
                             ids=['empty_str', 'space'])
    async def test_select_user_empty_id(self, websocket_connection, id):
        with allure.step('Select phone from db'):
            request_data = {
                'id': id,
                'method': 'select',
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id') == request_data.get('id')
            assert response_json.get('method') == request_data.get('method')
            assert response_json.get('status') == 'failure'

    @allure.description('Check select with incorrect method')
    @pytest.mark.asyncio
    @pytest.mark.parametrize('method', [None, '',  ' ', '&8()', 'get'],
                             ids=['None', 'empty str', 'space', 'symbols', 'non_exist_method'])
    async def test_select_user_incorrect_method(self, websocket_connection, method):
        with allure.step('Select phone from db'):
            request_data = {
                'id': get_id(),
                'method': method,
                'phone': generate_phone()
            }
            await websocket_connection.send(json.dumps(request_data))
            response = await websocket_connection.recv()
            response_json = json.loads(response)
        with allure.step('Check answer'):
            assert response_json.get('id', None) == request_data.get('id')
            assert response_json.get('method') == request_data.get('method'), 'error message should contain method'
            assert response_json.get('status') == 'failure'
